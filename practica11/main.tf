provider "google" {
  credentials = file("airy-media-291300-5f72d6eae119.json")
  project = "airy-media-291300"
  region = "us-west1"

}

resource "random_id" "instance_id" {
  byte_length = 8
}

//crea maquina virtual
resource "google_compute_instance" "default" {
  name = "demo-terraform-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone = "us-west1-a"
  boot_disk {
    initialize_params{
      image="debian-cloud/debian-9"

    }
  }

  metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

  network_interface {
    network= "default"

    access_config{

    }
  }

}